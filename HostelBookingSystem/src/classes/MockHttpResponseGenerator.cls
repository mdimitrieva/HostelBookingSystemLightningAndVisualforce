@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        System.assertEquals('http://api.openweathermap.org/data/2.5/weather?q=Plovdiv&units=metric&appID=3485ff314c0d46bbfabee923cdfae25e', req.getEndpoint());
        System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"coord":{"lon":-0.13,"lat":51.51},"weather":[{"id":721,"main":"Haze","description":"haze","icon":"50d"}],"base":"stations","main":{"temp":291.33,"pressure":1014,"humidity":68,"temp_min":289.15,"temp_max":293.15},"visibility":10000,"wind":{"speed":1},"clouds":{"all":56},"dt":1503993000,"sys":{"type":1,"id":5091,"message":0.0029,"country":"GB","sunrise":1503983341,"sunset":1504032749},"id":2643743,"name":"Plovdiv","cod":200}');
        res.setStatusCode(200);
        return res;
    }
}