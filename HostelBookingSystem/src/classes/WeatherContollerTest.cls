@isTest
private class WeatherContollerTest {

	@isTest
	private static void testName() {
		
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());

		ReservationController resController = new ReservationController();
		WeatherController wController = new WeatherController(resController);

		HttpResponse res = WeatherController.getResponse();
        Test.startTest(); 
        // Verify response received contains fake values
        String contentType = res.getHeader('Content-Type');
        System.assert(contentType == 'application/json');
        String actualValue = res.getBody();
        String expectedValue = '{"coord":{"lon":-0.13,"lat":51.51},"weather":[{"id":721,"main":"Haze","description":"haze","icon":"50d"}],"base":"stations","main":{"temp":291.33,"pressure":1014,"humidity":68,"temp_min":289.15,"temp_max":293.15},"visibility":10000,"wind":{"speed":1},"clouds":{"all":56},"dt":1503993000,"sys":{"type":1,"id":5091,"message":0.0029,"country":"GB","sunrise":1503983341,"sunset":1504032749},"id":2643743,"name":"Plovdiv","cod":200}';
        System.assertEquals(actualValue, expectedValue);
        System.assertEquals(200, res.getStatusCode());

		Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(expectedValue);
		wController.city = String.valueOf(results.get('name'));		
		System.assertEquals('Plovdiv', wController.city);

		Map<String, Object> mainResults = (Map<String, Object>) (results.get('main'));
		wController.temp = String.valueOf(mainResults.get('temp'));
		System.assertEquals('291.33', wController.temp );
		wController.pressure = String.valueOf(mainResults.get('pressure'));
		System.assertEquals('1014',wController.pressure );
		wController.humidity =  String.valueOf(mainResults.get('humidity'));
		System.assertEquals('68',wController.humidity);
		wController.temp_min =String.valueOf(mainResults.get('temp_min'));
		System.assertEquals('289.15',wController.temp_min );
		wController.temp_max =String.valueOf(mainResults.get('temp_max'));
		System.assertEquals('293.15', wController.temp_max);

		Test.stopTest(); 
	
	}
}