public with sharing class LightningReservationControllerExtension {

    @AuraEnabled
    public static List<Reservation__c> findByName(String searchKey) {
        String name = '%' + searchKey + '%';
        return [SELECT Id,Name, Date_Occupied__c, Relese_Date__c, Room__c, Duration__c from Reservation__c WHERE name LIKE :name LIMIT 50];
    } 
    
    @AuraEnabled
 	public static PagerWrapper pagingResults(Decimal pageNumber ,Integer recordToDisply,String sortBy,boolean isAsc) {
      Integer pageSize = recordToDisply;
      Integer offset = ((Integer)pageNumber - 1) * pageSize;        
    
   	  PagerWrapper obj =  new PagerWrapper();

        obj.pageSize = pageSize;
        obj.page = (Integer) pageNumber;
        obj.total = [SELECT count() FROM Reservation__c];      
        obj.reservations = [SELECT Id,Name, Date_Occupied__c, Relese_Date__c, Room__c, Duration__c from Reservation__c  LIMIT :recordToDisply OFFSET :offset];
		

        return obj;
     }
       
     @AuraEnabled
    public static Reservation__c remove(Reservation__c reservation) {
        delete reservation;
        return reservation;
    }
	
    public class PagerWrapper {
        @AuraEnabled 
        public Integer pageSize {get;set;}
        @AuraEnabled 
        public Integer page {get;set;}
        @AuraEnabled 
        public Integer total {get;set;}
        @AuraEnabled 
        public List<Reservation__c> reservations {get;set;}  
   }    
}