public class LightningChartController {
	
	@AuraEnabled
	public static List<WrapperData> getChartData() {
		List<WrapperData> data = new List<WrapperData> ();
		List<AggregateResult> counts = [SELECT DAY_IN_WEEK(Date_Occupied__c), Count(Name) from Reservation__c where Date_Occupied__c = THIS_WEEK  group by DAY_IN_WEEK(Date_Occupied__c)];

		for (AggregateResult a : counts) {
			if (a.get('expr0') == 2) {
				data.add(new WrapperData('Monday', (Integer) a.get('expr1')));
			} else if (a.get('expr0') == 3) {
				data.add(new WrapperData('Tuesday', (Integer) a.get('expr1')));
			} else if (a.get('expr0') == 4) {
				data.add(new WrapperData('Wednesday', (Integer) a.get('expr1')));
			} else if (a.get('expr0') == 5) {
				data.add(new WrapperData('Thursday', (Integer) a.get('expr1')));
			} else if (a.get('expr0') == 6) {
				data.add(new WrapperData('Friday', (Integer) a.get('expr1')));
			} else if (a.get('expr0') == 7) {
				data.add(new WrapperData('Saturday', (Integer) a.get('expr1')));
			} else if (a.get('expr0') == 1) {
				data.add(new WrapperData('Sunday', (Integer) a.get('expr1')));
			}

		}
		return data;
	}

	public class WrapperData {
        
        @AuraEnabled
		public String dayOfWeek { get; set; }
        @AuraEnabled
		public Integer numOfResPerDay { get; set; }
		public WrapperData(String name, Integer num) {
			this.dayOfWeek = name;
			this.numOfResPerDay = num;
		}
	}
}