@isTest
public class ReservationContollerTest {

	static ReservationController controller = new ReservationController();

	static testmethod void saveTest() {

		List<Reservation__c> reservations = new List<Reservation__c> ();
		Date startDate = Date.today().addDays(1);
		Date endDate = Date.today().addDays(3);
		List<Room__c> rooms = new List<Room__c> ();
		Hostel__c hostel = new Hostel__c(Name = 'Hostel', Address__c = 'Hostel', Email__c = 'hostel@gmail.com');
		insert hostel;
		List<Hostel__c> hostels = [select Id, Name from Hostel__c];
		for (Integer i = 0; i< 5; i++) {
			Room__c r = new Room__c(Name = 'Room__c ' + i, Capacity__c = i + 1, IsAvailable__c = false, Hostel__c = hostels.get(0).Id);
			rooms.add(r);
		}
		insert rooms;
		List<Room__c> rom = [select Id, Name, IsAvailable__c from Room__c];
		for (Integer i = 0; i< 5; i++) {
			Reservation__c r = new Reservation__c(Name = 'Reservation__c ' + i, Date_Occupied__c = startDate, Relese_Date__c = endDate, Room__c = rom.get(i).Id);
			reservations.add(r);
		}

		for (Reservation__c res : reservations) {
			controller.reservation = res;
			controller.save();
		}
		for (Integer i = 0; i<rooms.size(); i++) {
			rooms.get(i).IsAvailable__c = false;

		}
		update(rooms);
		List<Reservation__c> allReservations = [SELECT Id FROM Reservation__c];
		Map<Id, Reservation__c> resMap = new Map<Id, Reservation__c> (allReservations);
		List<Id> resIds = new List<Id> (resMap.keySet());

		Test.startTest();

		System.assertEquals(5, resIds.size(), 'Check for save method ');

		Reservation__c r = new Reservation__c(Name = 'Reservation__c ', Date_Occupied__c = startDate, Relese_Date__c = endDate, Room__c = rom.get(0).Id);
		controller.reservation = r;
		controller.save();
		resIds.add(controller.reservation.Id);

		List<Reservation__c> lr = [SELECT Id FROM Reservation__c WHERE Id IN :resIds];
		System.assertEquals(5, lr.size(), 'Check for save method with validation');

		delete[SELECT Name, Date_Occupied__c, Relese_Date__c, Room__c, Duration__c from Reservation__c where id = : resIds.get(0)];

		List<Reservation__c> dr = [SELECT Id FROM Reservation__c WHERE Id IN :resIds];

		System.assertEquals(4, dr.size(), 'Check for delete method');

		Test.stopTest();
	}

	static testmethod void editTest() {

		List<Reservation__c> reservations = new List<Reservation__c> ();
		Date startDate = Date.today().addDays(1);
		Date endDate = Date.today().addDays(3);
		List<Room__c> rooms = new List<Room__c> ();
		Hostel__c hostel = new Hostel__c(Name = 'Hostel', Address__c = 'Hostel', Email__c = 'hostel@gmail.com');
		insert hostel;
		List<Hostel__c> hostels = [select Id, Name from Hostel__c];
		for (Integer i = 0; i< 5; i++) {
			Room__c r = new Room__c(Name = 'Room__c ' + i, Capacity__c = i + 1, IsAvailable__c = false, Hostel__c = hostels.get(0).Id);
			rooms.add(r);
		}
		insert rooms;
		List<Room__c> rom = [select Id, Name, IsAvailable__c from Room__c];
		for (Integer i = 0; i< 5; i++) {
			Reservation__c r = new Reservation__c(Name = 'Reservation__c ' + i, Date_Occupied__c = startDate.addDays(i), Relese_Date__c = endDate.addDays(i), Room__c = rom.get(i).Id);
			reservations.add(r);
		}

		for (Reservation__c res : reservations) {
			controller.reservation = res;
			controller.save();
		}

		for (Integer i = 0; i<rooms.size(); i++) {
			rooms.get(i).IsAvailable__c = false;

		}
		update(rooms);

		Test.startTest();

		Reservation__c editRes = [SELECT Name FROM Reservation__c WHERE Name = 'Reservation__c 1'];
		editRes.Name = 'Edited Reservation';

		controller.reservation = editRes;
		controller.save().getParameters().put('id', controller.reservation.Id);

		System.assertEquals('Edited Reservation', controller.reservation.Name, 'Check for edit method');

		Reservation__c editReser = [SELECT Name FROM Reservation__c WHERE Name = 'Reservation__c 2'];
		controller.reservation = editReser;
		controller.reservation.Id = editReser.Id;
		controller.reservation.Relese_Date__c = endDate.addDays(- 6);
		controller.save().getParameters().put('id', controller.reservation.Id);

		System.assertEquals(editReser.Relese_Date__c, controller.reservation.Relese_Date__c, 'Check for edit method with validation');

		PageReference p = Page.HostelBookingSystem;
		p.getParameters().put('id', controller.reservation.Id);
		controller.edit();

		Test.stopTest();
	}

	static testmethod void viewTest() {
		String view = controller.view().getUrl();
		System.assertEquals(null, view, 'Check for view method');
	}

	static testmethod void getRecordsTest() {
		List<Reservation__c> results = controller.getRecords();
		System.assertNotEquals(null, results);
	}

	static testmethod void getRecordTest() {
		List<Reservation__c> reservations = new List<Reservation__c> ();
		Date startDate = Date.today().addDays(1);
		Date endDate = Date.today().addDays(3);
		List<Room__c> rooms = new List<Room__c> ();
		Hostel__c hostel = new Hostel__c(Name = 'Hostel', Address__c = 'Hostel', Email__c = 'hostel@gmail.com');
		insert hostel;
		List<Hostel__c> hostels = [select Id, Name from Hostel__c];
		for (Integer i = 0; i< 5; i++) {
			Room__c r = new Room__c(Name = 'Room__c ' + i, Capacity__c = i + 1, IsAvailable__c = false, Hostel__c = hostels.get(0).Id);
			rooms.add(r);
		}
		insert rooms;
		List<Room__c> rom = [select Id, Name, IsAvailable__c from Room__c];
		for (Integer i = 0; i< 5; i++) {
			Reservation__c r = new Reservation__c(Name = 'Reservation__c ' + i, Date_Occupied__c = startDate, Relese_Date__c = endDate, Room__c = rom.get(i).Id);
			reservations.add(r);
		}


		for (Reservation__c res : reservations) {
			controller.reservation = res;
			controller.save();
		}
		for (Integer i = 0; i<rooms.size(); i++) {
			rooms.get(i).IsAvailable__c = false;

		}
		update(rooms);

		Test.startTest();
		
		PageReference p = ApexPages.CurrentPage();
		p.getParameters().put('id', reservations.get(0).Id);
		Reservation__c result = (Reservation__c) controller.getRecord();
		System.assertNotEquals(null, result);

		Test.stopTest();
	}


	static testmethod void getIdTest() {
		String getId = ApexPages.currentPage().getParameters().get('id');
		System.assertEquals(null, getId);
	}

	static testmethod void removeTest() {
		List<Reservation__c> reservations = new List<Reservation__c> ();
		Date startDate = Date.today().addDays(1);
		Date endDate = Date.today().addDays(3);
		List<Room__c> rooms = new List<Room__c> ();
		Hostel__c hostel = new Hostel__c(Name = 'Hostel', Address__c = 'Hostel', Email__c = 'hostel@gmail.com');
		insert hostel;
		List<Hostel__c> hostels = [select Id, Name from Hostel__c];
		for (Integer i = 0; i< 5; i++) {
			Room__c r = new Room__c(Name = 'Room__c ' + i, Capacity__c = i + 1, IsAvailable__c = false, Hostel__c = hostels.get(0).Id);
			rooms.add(r);
		}
		insert rooms;
		List<Room__c> rom = [select Id, Name, IsAvailable__c from Room__c];
		for (Integer i = 0; i< 5; i++) {
			Reservation__c r = new Reservation__c(Name = 'Reservation__c ' + i, Date_Occupied__c = startDate, Relese_Date__c = endDate, Room__c = rom.get(i).Id);
			reservations.add(r);
		}


		for (Reservation__c res : reservations) {
			controller.reservation = res;
			controller.save();
		}
		for (Integer i = 0; i<rooms.size(); i++) {
			rooms.get(i).IsAvailable__c = false;

		}
		update(rooms);

		Test.startTest();
		Map<Id, Reservation__c> resMap = new Map<Id, Reservation__c> (reservations);
		List<Id> resIds = new List<Id> (resMap.keySet());
		Reservation__c r = [SELECT Name, Date_Occupied__c, Relese_Date__c, Room__c, Duration__c from Reservation__c where id = :resIds.get(0)];

		PageReference p = ApexPages.CurrentPage();
		p.getParameters().put('id', r.Id);
		controller.remove();
		List<Reservation__c> results = [Select Id from Reservation__c];

		System.assertNotEquals(resIds.size(), results.size(), 'Check for delete method');		

		PageReference page = ApexPages.CurrentPage();
		page.getParameters().put('id', r.Id);
		controller.remove();
		List<Reservation__c> checkresults = [Select Id from Reservation__c];

		System.assertNotEquals(resIds.size(), checkresults.size(), 'Check for delete method');


		Test.stopTest();
	}

	static testmethod void cancelTest() {
		System.assertNotEquals('/apex/HostelBookingSystem', controller.cancel().getUrl());
	}

	static testmethod void allReservationsTest() {
		System.assertEquals(controller.allReservations,[SELECT Name, Date_Occupied__c, Relese_Date__c, Room__c, Duration__c from Reservation__c]);
	}
	
}