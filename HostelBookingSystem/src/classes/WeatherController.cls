public with sharing class WeatherController {

	public String city { get; set; }
	public String temp { get; set; }
	public String pressure { get; set; }
	public String humidity { get; set; }
	public String temp_min { get; set; }
	public String temp_max { get; set; }

	public WeatherController(ReservationController resController) {
	// If the request is successful, parse the JSON response.
		if (getResponse().getStatusCode() == 200) {

			// Deserialize the JSON string into collections of primitive data types.
			Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(getResponse().getBody());
			city = String.valueOf(results.get('name'));

			Map<String, Object> mainResults = (Map<String, Object>) (results.get('main'));
			temp = String.valueOf(mainResults.get('temp'));
			pressure = String.valueOf(mainResults.get('pressure'));
			humidity = String.valueOf(mainResults.get('humidity'));
			temp_min = String.valueOf(mainResults.get('temp_min'));
			temp_max = String.valueOf(mainResults.get('temp_max'));

		} else {
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'There was an error retrieving the weather information.');
			ApexPages.addMessage(myMsg);
		}

	}

	public static HttpResponse getResponse() {
		String city = 'Plovdiv';
		String apiKey = '3485ff314c0d46bbfabee923cdfae25e';

		String requestEndpoint = 'http://api.openweathermap.org/data/2.5/weather';
		requestEndpoint += '?q=' + city + '&units=metric' + '&appID=' + apiKey;

		Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setEndpoint(requestEndpoint);
		request.setMethod('GET');
		HttpResponse response = http.send(request);

		return response;
	}

}