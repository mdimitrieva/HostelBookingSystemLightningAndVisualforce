public class ReservationControllerExtension {

	public String searchText { get; set; }
	public Reservation__c res { get; set; }
	public String ascendingOrDescending = ' ASC ';
	public List<Reservation__c> results {get;set;}
	public List<Reservation__c> reservationsToShow { get; set; }
	public Integer rec { get; set; }
	public Integer numOfPages { get; set; }

	public Integer counter = 0; //TO track the number of records parsed
	Integer limitSize = 2; //Number of records to be displayed
	public Integer totalSize = 0; //To Store the total number of records available

	//public List<Reservation__c> getResults() {
		//return results;
	//}
	public PageReference doSearch() {

		if (searchText.length() == 0) {
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'The search field must be more than 2 characters long!');
			ApexPages.addMessage(myMsg);
		} else if (searchText.length() <= 1) {
			results = (List<Reservation__c>) [SELECT Name, Date_Occupied__c, Relese_Date__c, Room__c, Duration__c from Reservation__c WHERE Date_Occupied__c = THIS_MONTH];

		} else if (res.Date_Occupied__c == null & res.Relese_Date__c == null) {
			results = (List<Reservation__c>) [FIND :searchText IN ALL FIELDS RETURNING Reservation__c(Name, Date_Occupied__c, Relese_Date__c, Room__c, Duration__c WHERE Date_Occupied__c = THIS_MONTH)] [0];

		}
		else if (searchText != null & res.Date_Occupied__c != null & res.Relese_Date__c == null) {
			results = (List<Reservation__c>) [FIND :searchText IN ALL FIELDS RETURNING Reservation__c(Name, Date_Occupied__c, Relese_Date__c, Room__c, Duration__c WHERE Date_Occupied__c = :res.Date_Occupied__c)] [0];

		}
		else {
			results = (List<Reservation__c>) [FIND :searchText IN ALL FIELDS RETURNING Reservation__c(Name, Date_Occupied__c, Relese_Date__c, Room__c, Duration__c WHERE Date_Occupied__c = :res.Date_Occupied__c and Relese_Date__c = :res.Relese_Date__c)] [0];

		}
		if (!results.isEmpty()) {
			reservationsToShow.clear();
			totalSize = results.size();
			numOfPages = (math.mod(totalSize, limitSize) > 0)
			? totalSize / limitSize + 1
			: (totalSize / limitSize);
			if ((counter + limitSize) <= totalSize) {
				for (Integer i = 0; i<limitSize; i++) {
					reservationsToShow.add(results.get(i));
				}
			} else {
				for (Integer i = 0; i<totalSize; i++) {
					reservationsToShow.add(results.get(i));
				}
			}
		}

		return null;
	}

	public ReservationControllerExtension(){
	}

	public ReservationControllerExtension(ReservationController resController) {
		res = new Reservation__c();
		results = resController.getRecords();
		totalSize = results.size();
		numOfPages = (math.mod(totalSize, limitSize)> 0)
		? totalSize / limitSize + 1
		: (totalSize / limitSize);


		reservationsToShow = new List<Reservation__c> ();
		if ((counter + limitSize) <= totalSize) {
			for (Integer i = 0; i<limitSize; i++) {
				reservationsToShow.add(results.get(i));
			}
		} else {
			for (Integer i = 0; i<totalSize; i++) {
				reservationsToShow.add(results.get(i));
			}
		}
	}

	public void sortByName() {
		Map<Id, Reservation__c> resMap = new Map<Id, Reservation__c> (reservationsToShow);
		List<Id> resIds = new List<Id> (resMap.keySet());

		if (ascendingOrDescending == ' ASC ') {
			reservationsToShow = (List<Reservation__c>) [SELECT Name, Date_Occupied__c, Relese_Date__c, Room__c, Duration__c from Reservation__c where Id in :resIds ORDER BY Name ASC];
			ascendingOrDescending = ' DESC ';
		} else {
			reservationsToShow = (List<Reservation__c>) [SELECT Name, Date_Occupied__c, Relese_Date__c, Room__c, Duration__c from Reservation__c where Id in :resIds ORDER BY Name DESC];
			ascendingOrDescending = ' ASC ';
		}
	}

	public void sortByOccupiedDate() {
		Map<Id, Reservation__c> resMap = new Map<Id, Reservation__c> (reservationsToShow);
		List<Id> resIds = new List<Id> (resMap.keySet());
		
		if (ascendingOrDescending == ' ASC ') {
			reservationsToShow = (List<Reservation__c>) [SELECT Name, Date_Occupied__c, Relese_Date__c, Room__c, Duration__c from Reservation__c where Id in :resIds ORDER BY Date_Occupied__c ASC];
			ascendingOrDescending = ' DESC ';
		} else {
			reservationsToShow = (List<Reservation__c>) [SELECT Name, Date_Occupied__c, Relese_Date__c, Room__c, Duration__c from Reservation__c where Id in :resIds ORDER BY Date_Occupied__c DESC];
			ascendingOrDescending = ' ASC ';
		}
	}

	public void sortByRelesaDate() {
		Map<Id, Reservation__c> resMap = new Map<Id, Reservation__c> (reservationsToShow);
		List<Id> resIds = new List<Id> (resMap.keySet());

		if (ascendingOrDescending == ' ASC ') {
			reservationsToShow = (List<Reservation__c>) [SELECT Name, Date_Occupied__c, Relese_Date__c, Room__c, Duration__c from Reservation__c where Id in :resIds ORDER BY Relese_Date__c ASC];
			ascendingOrDescending = ' DESC ';
		} else {
			reservationsToShow = (List<Reservation__c>) [SELECT Name, Date_Occupied__c, Relese_Date__c, Room__c, Duration__c from Reservation__c where Id in :resIds ORDER BY Relese_Date__c DESC];
			ascendingOrDescending = ' ASC ';
		}
	}

	public void sortByDuration() {
		Map<Id, Reservation__c> resMap = new Map<Id, Reservation__c> (reservationsToShow);
		List<Id> resIds = new List<Id> (resMap.keySet());

		if (ascendingOrDescending == ' ASC ') {
			reservationsToShow = (List<Reservation__c>) [SELECT Name, Date_Occupied__c, Relese_Date__c, Room__c, Duration__c from Reservation__c where Id in :resIds ORDER BY Duration__c ASC];
			ascendingOrDescending = ' DESC ';
		} else {
			reservationsToShow = (List<Reservation__c>) [SELECT Name, Date_Occupied__c, Relese_Date__c, Room__c, Duration__c from Reservation__c where Id in :resIds ORDER BY Duration__c DESC];
			ascendingOrDescending = ' ASC ';
		}
	}

	public void updatePage() {
		limitSize = rec;
		if (!results.isEmpty()) {
			reservationsToShow.clear();
			totalSize = results.size();
			numOfPages = (math.mod(totalSize, limitSize)> 0)
			? totalSize / limitSize + 1
			: (totalSize / limitSize);
			if ((counter + limitSize) <= totalSize) {
				for (Integer i = 0; i<limitSize; i++) {
					reservationsToShow.add(results.get(i));
				}
			} else {
				for (Integer i = 0; i<totalSize; i++) {
					reservationsToShow.add(results.get(i));
				}
			}
		}
	}


	public void beginning() {

		reservationsToShow.clear();
		counter = 0;
		if ((counter + limitSize) <= totalSize) {
			for (Integer i = 0; i<limitSize; i++) {
				reservationsToShow.add(results.get(i));
			}

		} else {
			for (Integer i = 0; i<totalSize; i++) {
				reservationsToShow.add(results.get(i));
			}
		}

	}

	public void next() {

		reservationsToShow.clear();
		counter = counter + limitSize;
		if ((counter + limitSize) <= totalSize) {
			for (Integer i = counter; i<(counter + limitSize); i++) {
				reservationsToShow.add(results.get(i));
			}
		} else {
			for (Integer i = counter; i<totalSize; i++) {
				reservationsToShow.add(results.get(i));
			}
		}
	}

	public void previous() {

		reservationsToShow.clear();
		counter = counter - limitSize;
		for (Integer i = counter; i<(counter + limitSize); i++) {
			reservationsToShow.add(results.get(i));
		}
	}

	public void last() {

		reservationsToShow.clear();
		if (math.mod(totalSize, limitSize) == 0) {
			counter = limitSize * ((totalSize / limitSize) - 1);
		} else if (math.mod(totalSize, limitSize) != 0) {
			counter = limitSize * ((totalSize / limitSize));
		}
		for (Integer i = counter - 1; i<totalSize - 1; i++) {
			reservationsToShow.add(results.get(i));
		}

	}

	public Boolean getDisableNext() {

		if ((counter + limitSize) >= totalSize)
		return true;
		else
		return false;

	}

	public Boolean getDisablePrevious() {

		if (counter == 0)
		return true;
		else
		return false;
	}

}