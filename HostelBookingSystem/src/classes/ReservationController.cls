public class ReservationController {

	public Reservation__c reservation { get; set; }
	public User user{
		get{
		return [SELECT Name from User where Id =: UserInfo.getUserId() ];
		}
		set;
	}

	public List<Reservation__c> allReservations {
		get {
			List<Reservation__c> result = [SELECT Name, Date_Occupied__c, Relese_Date__c, Room__c, Duration__c from Reservation__c];
			return result;
		}
		set;
	}

	public ReservationController() {
		Id reservationId = getId();
		reservation = (reservationId == null)
		? new Reservation__c()
		:[SELECT Name, Date_Occupied__c, Relese_Date__c, Room__c from Reservation__c where id = :reservationId];
	}

	public PageReference save() {
		try {
			if (reservation.Id == null) {
				if (reservation.Date_Occupied__c > System.today() & reservation.Relese_Date__c > System.today() & reservation.Relese_Date__c > reservation.Date_Occupied__c) {
					if (isFree(reservation)) {
						Room__c room = [select Name, IsAvailable__c from Room__c where Id = :reservation.Room__c];
						room.IsAvailable__c = false;
						update(room);
						upsert(reservation);

					} else {
						ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'This date is allready taken. Please choose another one!');
						ApexPages.addMessage(myMsg);
					} }
				else {
					ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid date. Please choose another one!');
					ApexPages.addMessage(myMsg);
				}
			} else {
				List<Reservation__c> res = [SELECT Name, Date_Occupied__c, Relese_Date__c, Room__c from Reservation__c where Date_Occupied__c != :reservation.Date_Occupied__c and Relese_Date__c != :reservation.Relese_Date__c];
				Boolean isTakend = false;
				for (Reservation__c r : res) {
					if (reservation.Date_Occupied__c == r.Date_Occupied__c & reservation.Date_Occupied__c <= r.Relese_Date__c) {
						isTakend = true;
					}
				}
				if (isTakend) {
					ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'This date is allready taken. Please choose another one!');
					ApexPages.addMessage(myMsg);
				} else {
					update(reservation);
				}
			}

		} catch(System.DMLException e) {
			ApexPages.addMessages(e);
			return null;
		}

		PageReference returnPage = ApexPages.currentPage();
		return returnPage;
	}

	public PageReference cancel() {
		return view();
	}

	public PageReference remove() {
		try {
			delete[SELECT Name, Date_Occupied__c, Relese_Date__c, Room__c, Duration__c from Reservation__c where id = : getId()];

		} catch(System.DMLException e) {
			ApexPages.addMessages(e);
			return null;
		}
		return view();
	}

	public PageReference edit() {
		reservation = (Reservation__c) getRecord();
		PageReference redirectSuccess = ApexPages.currentPage();
		redirectSuccess.getParameters().put('id',reservation.Id);
		return redirectSuccess;
	}

	public String getId() {
		return ApexPages.currentPage().getParameters().get('id');
	}

	public SObject getRecord() {
		return[SELECT Name, Date_Occupied__c, Relese_Date__c, Room__c from Reservation__c where id = : getId()];
	}

	public List<SObject> getRecords() {
		return[SELECT Name, Date_Occupied__c, Relese_Date__c, Room__c, Duration__C from Reservation__c];
	}

	public PageReference view() {
		PageReference returnPage = ApexPages.currentPage();
		returnPage.setRedirect(true).getParameters().clear();
		return returnPage;
	}

	public Boolean isFree(Reservation__c reservation) {
		List<Reservation__c> res = [select Date_Occupied__c, Relese_Date__c from Reservation__c where Room__c = :reservation.Room__c and(Date_Occupied__c = :reservation.Date_Occupied__c or Relese_Date__c >= :reservation.Date_Occupied__c)];
		if (res.isEmpty())
		return true;
		else
		return false;

	}
}