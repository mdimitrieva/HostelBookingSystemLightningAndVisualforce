public class LightningWeatherController {
    
    public static final String apiKey = '3485ff314c0d46bbfabee923cdfae25e'; 
    
   @AuraEnabled
    public static WrapperData getWeather(){
        LightningWeatherController.WrapperData weather = new  LightningWeatherController.WrapperData ();
        // define a response to caller
        String outcomeMsg;
        
        // define basic information for later, store these in a protected custom setting
        String endpoint = 'http://api.openweathermap.org/data/2.5/weather'; // be sure this is configured in "Remote Site Settings"
        String resource = '?q=' + 'Plovdiv' + '&units=metric' + '&appID=' + apiKey;
        String method = 'GET';  
        
               
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(endpoint + resource);
            req.setMethod(method);
           
            try {                
                
              HttpResponse res = h.send(req);
                
                // check response 
                if (res.getStatusCode() == 200) {
                    
                   	Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
                    
                    weather.city = String.valueOf(results.get('name'));
        
                    Map<String, Object> mainResults = (Map<String, Object>) (results.get('main'));
                    weather.temp = String.valueOf(mainResults.get('temp'));
                    weather.pressure = String.valueOf(mainResults.get('pressure'));
                    weather.humidity = String.valueOf(mainResults.get('humidity'));
                    weather.temp_min = String.valueOf(mainResults.get('temp_min'));
                    weather.temp_max = String.valueOf(mainResults.get('temp_max'));                               
                   
                } else {
                    // callout failed
                    outcomeMsg = 'Error: Callout failed. Please review the debug log for additional details.';
                }
                
            } catch (DMLexception e) {
                // Unexpected exceptions will be caught here, like a deserialization error.
                outcomeMsg = 'Error: An exception has been encountered while calling out to Integration:  ' + e.getMessage();
            }
        
        
        // Return the response
        return weather;
    }
    
    public class WrapperData {
         @AuraEnabled
		public String city { get; set; } 
         @AuraEnabled
        public String temp { get; set; } 
         @AuraEnabled
        public String pressure { get; set; }
         @AuraEnabled
        public String humidity { get; set; }
         @AuraEnabled
        public String temp_min { get; set; }
         @AuraEnabled
        public String temp_max { get; set; }
	}     
}