@isTest
private class ChartControllerExtensionTest {

	@isTest
	private static void testName() {
		
		ReservationController controller = new ReservationController();
		List<Reservation__c> reservations = new List<Reservation__c> ();
		Date startDate = Date.today().addDays(-2);
		Date endDate = Date.today().addDays(3);
		List<Room__c> rooms = new List<Room__c> ();
		Hostel__c hostel = new Hostel__c(Name = 'Hostel', Address__c = 'Hostel', Email__c = 'hostel@gmail.com');
		insert hostel;
		List<Hostel__c> hostels = [select Id, Name from Hostel__c];
		for (Integer i = 0; i< 5; i++) {
			Room__c r = new Room__c(Name = 'Room__c ' + i, Capacity__c = i + 1, IsAvailable__c = false, Hostel__c = hostels.get(0).Id);
			rooms.add(r);
		}
		insert rooms;
		List<Room__c> rom = [select Id, Name, IsAvailable__c from Room__c];
		for (Integer i = 0; i< 5; i++) {
			Reservation__c r = new Reservation__c(Name = 'Reservation__c ' + i, Date_Occupied__c = startDate + i, Relese_Date__c = endDate, Room__c = rom.get(i).Id);
			reservations.add(r);
		}
		for (Integer i = 0; i< 3; i++) {
			Reservation__c r = new Reservation__c(Name = 'Reservation__c ' + i, Date_Occupied__c = startDate.addDays(-i), Relese_Date__c = endDate, Room__c = rom.get(i).Id);
			reservations.add(r);
		}

		for (Reservation__c res : reservations) {
			controller.reservation = res;
			controller.save();
		}
		for (Integer i = 0; i<rooms.size(); i++) {
			rooms.get(i).IsAvailable__c = false;

		}
		update(rooms);
		
		Test.startTest();

		ChartControllerExtension chart = new ChartControllerExtension();
		ChartControllerExtension.WrapperData[] data = chart.getData();

		System.assertNotEquals(null,data);

		Test.stopTest();
	}
}