global class DailyApexJob implements Schedulable {
	/**
	 * @description Executes the scheduled Apex job. 
	 * @param sc contains the job ID
	 */
	global void execute(SchedulableContext sc) {
		List<Reservation__c> reservations = [SELECT Name, Date_Occupied__c, Relese_Date__c, Room__c, Duration__c from Reservation__c where Relese_Date__c = :System.today()];

		for (Reservation__c res : reservations) {
			Room__c room = [select Name, IsAvailable__c from Room__c where Id = :res.Room__c];
			room.IsAvailable__c = true;
			upsert(room);
			delete res;
		}
	}
}