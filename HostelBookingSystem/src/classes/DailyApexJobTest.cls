@isTest
public class DailyApexJobTest {

	@isTest
	public static void testName() {
		List<Reservation__c> reservations = new List<Reservation__c> ();
		Date startDate = Date.today().addDays(-7);
		Date endDate = Date.today().addDays(-3);
		List<Room__c> rooms = new List<Room__c> ();
		Hostel__c hostel = new Hostel__c(Name = 'Hostel', Address__c = 'Hostel', Email__c = 'hostel@gmail.com');
		insert hostel;
		List<Hostel__c> hostels = [select Id, Name from Hostel__c];
		for (Integer i = 0; i< 5; i++) {
			Room__c r = new Room__c(Name = 'Room__c ' + i, Capacity__c = i + 1, IsAvailable__c = false, Hostel__c = hostels.get(0).Id);
			rooms.add(r);
		}
		insert rooms;

		List<Room__c> rom = [select Id, Name, IsAvailable__c from Room__c];
		for (Integer i = 0; i< 4; i++) {
			Reservation__c r = new Reservation__c(Name = 'Reservation__c ' + i, Date_Occupied__c = startDate.addDays(i), Relese_Date__c = endDate.addDays(i), Room__c = rom.get(i).Id);
			reservations.add(r);
		}
		insert reservations;

		for (Integer i = 0; i<rooms.size(); i++) {
			rooms.get(i).IsAvailable__c = false;

		}
		update(rooms);


		Test.startTest();

		String CRON_EXP = Datetime.now().second().format()+' '+Datetime.now().minute().format()+' '+Datetime.now().hour().format()+' * * ?';

		String jobId = System.schedule('Daily cleaning rooms', CRON_EXP, new DailyApexJob());

		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

		System.assertEquals(CRON_EXP, ct.CronExpression);
		System.assertEquals(0, ct.TimesTriggered);
		//System.assertEquals(Datetime.now().second().format()+' '+Datetime.now().minute().format()+' '+Datetime.now().hour().format()+' * * ?', String.valueOf(ct.NextFireTime));

		Map<Id, Reservation__c> resMap = new Map<Id, Reservation__c> (reservations);
		List<Id> resIds = new List<Id> (resMap.keySet());

		List<Reservation__c> result = [select id from Reservation__c where Id =: resIds];
		System.assertEquals(resIds.size(),result.size());

		Test.stopTest();

		System.assertEquals(1, [SELECT count() FROM CronTrigger], 'A job should be scheduled');
		
		System.assertNotEquals(result.size(),resIds.size());

	}
}