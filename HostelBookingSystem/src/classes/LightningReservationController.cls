public with sharing class LightningReservationController {

    public static String successMsg {get;set;}
    public static String errorMsg {get;set;}
    
    @AuraEnabled
    public static List<Reservation__c> getReservations() {
		return[SELECT Id,Name, Date_Occupied__c, Relese_Date__c, Room__c, Duration__C from Reservation__c];
	}
    
    @AuraEnabled
    public static Reservation__c getReservation(Id id) {
		return[SELECT Id,Name, Date_Occupied__c, Relese_Date__c, Room__c from Reservation__c where Id = : id];
	}
        
    @AuraEnabled
    public static String save(Reservation__c reservation) {
        try {
          if(reservation.Name ==''){
              successMsg='';
          return errorMsg ='Please input valid name!';
        } else if (reservation.Id == null) {
				if (reservation.Date_Occupied__c > System.today() & reservation.Relese_Date__c > System.today() & reservation.Relese_Date__c > reservation.Date_Occupied__c) {
					if (isFree(reservation)) {
						Room__c room = [select Name, IsAvailable__c from Room__c where Id = :reservation.Room__c];
						room.IsAvailable__c = false;
						update(room);
						upsert(reservation);
                        errorMsg='';
                       return successMsg = 'Reservation saved successfull!';

					} else {
                        successMsg='';
						return errorMsg ='This date is allready taken. Please choose another one!';
						
					} }
				else {
                    successMsg='';
					return errorMsg = 'Invalid date. Please choose another one!';

				}
			} else {
                if (reservation.Date_Occupied__c > System.today() & reservation.Relese_Date__c > System.today() & reservation.Relese_Date__c > reservation.Date_Occupied__c) {

                    List<Reservation__c> res = [SELECT Name, Date_Occupied__c, Relese_Date__c, Room__c from Reservation__c where Date_Occupied__c != :reservation.Date_Occupied__c and Relese_Date__c != :reservation.Relese_Date__c];
                    Boolean isTakend = false;
                    for (Reservation__c r : res) {
                        if (reservation.Date_Occupied__c == r.Date_Occupied__c & reservation.Date_Occupied__c <= r.Relese_Date__c) {
                            isTakend = true;
                        }
                    }
                    if (isTakend) {
                        successMsg='';
                        return errorMsg ='This date is allready taken. Please choose another one!';
    
                    } else {
                        update(reservation);
                         errorMsg='';
                        return successMsg = 'Reservation saved successfull!';

                	}
                } else {
                    return errorMsg ='Invalid date. Please choose another one!';

                }
                    
            }

		} catch(System.DMLException e) {
            successMsg='';
			return errorMsg = e.getMessage();
			
        }
       
    }
    
    public static Boolean isFree(Reservation__c reservation) {
		List<Reservation__c> res = [select Date_Occupied__c, Relese_Date__c from Reservation__c where Room__c = :reservation.Room__c and(Date_Occupied__c = :reservation.Date_Occupied__c or Relese_Date__c >= :reservation.Date_Occupied__c)];
		if (res.isEmpty())
		return true;
		else
		return false;

	}   
    
    @AuraEnabled
    public static List<Room__c> getRooms() {
		return[SELECT Name from Room__c];
	}
    
}