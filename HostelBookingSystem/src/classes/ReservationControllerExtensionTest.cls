@isTest
private class ReservationControllerExtensionTest {

	static ReservationController resController = new ReservationController();
	static ReservationControllerExtension controller = new ReservationControllerExtension(resController);

	@isTest
	private static void doSearchTest() {
		List<Reservation__c> reservations = new List<Reservation__c> ();
		Date startDate = Date.today().addDays(1);
		Date endDate = Date.today().addDays(3);
		List<Room__c> rooms = new List<Room__c> ();
		Hostel__c hostel = new Hostel__c(Name = 'Hostel', Address__c = 'Hostel', Email__c = 'hostel@gmail.com');
		insert hostel;
		List<Hostel__c> hostels = [select Id, Name from Hostel__c];
		for (Integer i = 0; i< 5; i++) {
			Room__c r = new Room__c(Name = 'Room__c ' + i, Capacity__c = i + 1, IsAvailable__c = false, Hostel__c = hostels.get(0).Id);
			rooms.add(r);
		}
		insert rooms;
		List<Room__c> rom = [select Id, Name, IsAvailable__c from Room__c];
		for (Integer i = 0; i< 5; i++) {
			Reservation__c r = new Reservation__c(Name = 'Reservation__c ' + i, Date_Occupied__c = startDate, Relese_Date__c = endDate, Room__c = rom.get(i).Id);
			reservations.add(r);
		}

		for (Reservation__c res : reservations) {
			resController.reservation = res;
			resController.save();
		}
		for (Integer i = 0; i<rooms.size(); i++) {
			rooms.get(i).IsAvailable__c = false;

		}
		update(rooms);

		Test.startTest();


		controller.searchText = 'Reservation__c 1';
		controller.doSearch();

		controller.searchText = '';
		controller.doSearch();

		controller.searchText = 'M';
		controller.doSearch();

		controller.searchText = 'Reservation__c 1';
		controller.res.Date_Occupied__c = Date.valueOf('2017-08-28');
		controller.doSearch();

		controller.searchText = 'Reservation__c 1';
		controller.res.Date_Occupied__c = Date.valueOf('2017-08-28');
		controller.res.Relese_Date__c = Date.valueOf('2017-09-03');
		controller.doSearch();

		Test.stopTest();

	}

	static testmethod void sortByNameTest() {
		List<Reservation__c> reservations = new List<Reservation__c> ();
		Date startDate = Date.today().addDays(1);
		Date endDate = Date.today().addDays(3);
		List<Room__c> rooms = new List<Room__c> ();
		Hostel__c hostel = new Hostel__c(Name = 'Hostel', Address__c = 'Hostel', Email__c = 'hostel@gmail.com');
		insert hostel;
		List<Hostel__c> hostels = [select Id, Name from Hostel__c];
		for (Integer i = 0; i< 5; i++) {
			Room__c r = new Room__c(Name = 'Room__c ' + i, Capacity__c = i + 1, IsAvailable__c = false, Hostel__c = hostels.get(0).Id);
			rooms.add(r);
		}
		insert rooms;
		List<Room__c> rom = [select Id, Name, IsAvailable__c from Room__c];
		for (Integer i = 0; i< 5; i++) {
			Reservation__c r = new Reservation__c(Name = 'Reservation__c ' + i, Date_Occupied__c = startDate, Relese_Date__c = endDate, Room__c = rom.get(i).Id);
			reservations.add(r);
		}

		for (Reservation__c res : reservations) {
			resController.reservation = res;
			resController.save();
		}
		for (Integer i = 0; i<rooms.size(); i++) {
			rooms.get(i).IsAvailable__c = false;

		}
		update(rooms);
		Test.startTest();

		controller.reservationsToShow = reservations;
		controller.sortByName();

		System.assertEquals('Reservation__c 0', controller.reservationsToShow.get(0).Name);

		controller.ascendingOrDescending = ' DESC ';
		controller.sortByName();

		System.assertEquals('Reservation__c 4', controller.reservationsToShow.get(0).Name);

		Test.stopTest();

	}

	static testmethod void sortByOccupiedDateTest() {
		List<Reservation__c> reservations = new List<Reservation__c> ();
		Date startDate = Date.today().addDays(1);
		Date endDate = Date.today().addDays(3);
		List<Room__c> rooms = new List<Room__c> ();
		Hostel__c hostel = new Hostel__c(Name = 'Hostel', Address__c = 'Hostel', Email__c = 'hostel@gmail.com');
		insert hostel;
		List<Hostel__c> hostels = [select Id, Name from Hostel__c];
		for (Integer i = 0; i< 5; i++) {
			Room__c r = new Room__c(Name = 'Room__c ' + i, Capacity__c = i + 1, IsAvailable__c = false, Hostel__c = hostels.get(0).Id);
			rooms.add(r);
		}
		insert rooms;
		List<Room__c> rom = [select Id, Name, IsAvailable__c from Room__c];
		for (Integer i = 0; i< 5; i++) {
			Reservation__c r = new Reservation__c(Name = 'Reservation__c ' + i, Date_Occupied__c = startDate.addDays(i), Relese_Date__c = endDate.addDays(i), Room__c = rom.get(i).Id);
			reservations.add(r);
		}

		for (Reservation__c res : reservations) {
			resController.reservation = res;
			resController.save();
		}
		for (Integer i = 0; i<rooms.size(); i++) {
			rooms.get(i).IsAvailable__c = false;

		}
		update(rooms);
		Test.startTest();

		controller.reservationsToShow = reservations;
		controller.sortByOccupiedDate();

		System.assertEquals(Date.today().addDays(1), controller.reservationsToShow.get(0).Date_Occupied__c);
		System.assertNotEquals(Date.today(), controller.reservationsToShow.get(0).Date_Occupied__c);

		controller.ascendingOrDescending = ' DESC ';
		controller.sortByOccupiedDate();

		System.assertEquals(Date.today().addDays(5), controller.reservationsToShow.get(0).Date_Occupied__c);
		System.assertNotEquals(Date.today(), controller.reservationsToShow.get(0).Date_Occupied__c);

		Test.stopTest();

	}

	static testmethod void sortByRelesaDateTest() {
		List<Reservation__c> reservations = new List<Reservation__c> ();
		Date startDate = Date.today().addDays(1);
		Date endDate = Date.today().addDays(3);
		List<Room__c> rooms = new List<Room__c> ();
		Hostel__c hostel = new Hostel__c(Name = 'Hostel', Address__c = 'Hostel', Email__c = 'hostel@gmail.com');
		insert hostel;
		List<Hostel__c> hostels = [select Id, Name from Hostel__c];
		for (Integer i = 0; i< 5; i++) {
			Room__c r = new Room__c(Name = 'Room__c ' + i, Capacity__c = i + 1, IsAvailable__c = false, Hostel__c = hostels.get(0).Id);
			rooms.add(r);
		}
		insert rooms;
		List<Room__c> rom = [select Id, Name, IsAvailable__c from Room__c];
		for (Integer i = 0; i< 5; i++) {
			Reservation__c r = new Reservation__c(Name = 'Reservation__c ' + i, Date_Occupied__c = startDate.addDays(i), Relese_Date__c = endDate.addDays(i), Room__c = rom.get(i).Id);
			reservations.add(r);
		}

		for (Reservation__c res : reservations) {
			resController.reservation = res;
			resController.save();
		}
		for (Integer i = 0; i<rooms.size(); i++) {
			rooms.get(i).IsAvailable__c = false;

		}
		update(rooms);
		Test.startTest();

		controller.reservationsToShow = reservations;
		controller.sortByRelesaDate();

		System.assertEquals(Date.today().addDays(3), controller.reservationsToShow.get(0).Relese_Date__c);
		System.assertNotEquals(Date.today(), controller.reservationsToShow.get(0).Relese_Date__c);

		controller.ascendingOrDescending = ' DESC ';
		controller.sortByRelesaDate();

		System.assertEquals(Date.today().addDays(7), controller.reservationsToShow.get(0).Relese_Date__c);
		System.assertNotEquals(Date.today(), controller.reservationsToShow.get(0).Relese_Date__c);

		Test.stopTest();

	}

	static testmethod void sortByDurationTest() {
		List<Reservation__c> reservations = new List<Reservation__c> ();
		Date startDate = Date.today().addDays(1);
		Date endDate = Date.today().addDays(3);
		List<Room__c> rooms = new List<Room__c> ();
		Hostel__c hostel = new Hostel__c(Name = 'Hostel', Address__c = 'Hostel', Email__c = 'hostel@gmail.com');
		insert hostel;
		List<Hostel__c> hostels = [select Id, Name from Hostel__c];
		for (Integer i = 0; i< 5; i++) {
			Room__c r = new Room__c(Name = 'Room__c ' + i, Capacity__c = i + 1, IsAvailable__c = false, Hostel__c = hostels.get(0).Id);
			rooms.add(r);
		}
		insert rooms;
		List<Room__c> rom = [select Id, Name, IsAvailable__c from Room__c];
		for (Integer i = 0; i< 5; i++) {
			Reservation__c r = new Reservation__c(Name = 'Reservation__c ' + i, Date_Occupied__c = startDate.addDays(i), Relese_Date__c = endDate.addDays(i), Room__c = rom.get(i).Id);
			reservations.add(r);
		}

		for (Reservation__c res : reservations) {
			resController.reservation = res;
			resController.save();
		}
		for (Integer i = 0; i<rooms.size(); i++) {
			rooms.get(i).IsAvailable__c = false;

		}
		update(rooms);
		Test.startTest();

		controller.reservationsToShow = reservations;
		controller.sortByDuration();

		System.assertEquals(2, controller.reservationsToShow.get(0).Duration__c);
		System.assertNotEquals(0, controller.reservationsToShow.get(0).Duration__c);

		controller.ascendingOrDescending = ' DESC ';
		controller.sortByDuration();

		System.assertEquals(2, controller.reservationsToShow.get(0).Duration__c);
		System.assertNotEquals(0, controller.reservationsToShow.get(0).Duration__c);

		Test.stopTest();

	}

	static testmethod void updatePageTest() {
		List<Reservation__c> reservations = new List<Reservation__c> ();
		Date startDate = Date.today().addDays(1);
		Date endDate = Date.today().addDays(3);
		List<Room__c> rooms = new List<Room__c> ();
		Hostel__c hostel = new Hostel__c(Name = 'Hostel', Address__c = 'Hostel', Email__c = 'hostel@gmail.com');
		insert hostel;
		List<Hostel__c> hostels = [select Id, Name from Hostel__c];
		for (Integer i = 0; i< 5; i++) {
			Room__c r = new Room__c(Name = 'Room__c ' + i, Capacity__c = i + 1, IsAvailable__c = false, Hostel__c = hostels.get(0).Id);
			rooms.add(r);
		}
		insert rooms;
		List<Room__c> rom = [select Id, Name, IsAvailable__c from Room__c];
		for (Integer i = 0; i< 4; i++) {
			Reservation__c r = new Reservation__c(Name = 'Reservation__c ' + i, Date_Occupied__c = startDate.addDays(i), Relese_Date__c = endDate.addDays(i), Room__c = rom.get(i).Id);
			reservations.add(r);
		}

		for (Reservation__c res : reservations) {
			resController.reservation = res;
			resController.save();
		}
		for (Integer i = 0; i<rooms.size(); i++) {
			rooms.get(i).IsAvailable__c = false;

		}
		update(rooms);

		Test.startTest();

		controller.results = reservations;
		//controller.totalSize = reservations.size();
		controller.updatePage();

		Test.stopTest();

	}

	static testmethod void nextTest() {
		List<Reservation__c> reservations = new List<Reservation__c> ();
		Date startDate = Date.today().addDays(1);
		Date endDate = Date.today().addDays(3);
		List<Room__c> rooms = new List<Room__c> ();
		Hostel__c hostel = new Hostel__c(Name = 'Hostel', Address__c = 'Hostel', Email__c = 'hostel@gmail.com');
		insert hostel;
		List<Hostel__c> hostels = [select Id, Name from Hostel__c];
		for (Integer i = 0; i< 5; i++) {
			Room__c r = new Room__c(Name = 'Room__c ' + i, Capacity__c = i + 1, IsAvailable__c = false, Hostel__c = hostels.get(0).Id);
			rooms.add(r);
		}
		insert rooms;
		List<Room__c> rom = [select Id, Name, IsAvailable__c from Room__c];
		for (Integer i = 0; i< 4; i++) {
			Reservation__c r = new Reservation__c(Name = 'Reservation__c ' + i, Date_Occupied__c = startDate.addDays(i), Relese_Date__c = endDate.addDays(i), Room__c = rom.get(i).Id);
			reservations.add(r);
		}

		for (Reservation__c res : reservations) {
			resController.reservation = res;
			resController.save();
		}
		for (Integer i = 0; i<rooms.size(); i++) {
			rooms.get(i).IsAvailable__c = false;

		}
		update(rooms);

		Test.startTest();

		controller.results = reservations;
		controller.next();

		Test.stopTest();
	}

	static testmethod void beginningTest() {
		List<Reservation__c> reservations = new List<Reservation__c> ();
		Date startDate = Date.today().addDays(1);
		Date endDate = Date.today().addDays(3);
		List<Room__c> rooms = new List<Room__c> ();
		Hostel__c hostel = new Hostel__c(Name = 'Hostel', Address__c = 'Hostel', Email__c = 'hostel@gmail.com');
		insert hostel;
		List<Hostel__c> hostels = [select Id, Name from Hostel__c];
		for (Integer i = 0; i< 5; i++) {
			Room__c r = new Room__c(Name = 'Room__c ' + i, Capacity__c = i + 1, IsAvailable__c = false, Hostel__c = hostels.get(0).Id);
			rooms.add(r);
		}
		insert rooms;
		List<Room__c> rom = [select Id, Name, IsAvailable__c from Room__c];
		for (Integer i = 0; i< 4; i++) {
			Reservation__c r = new Reservation__c(Name = 'Reservation__c ' + i, Date_Occupied__c = startDate.addDays(i), Relese_Date__c = endDate.addDays(i), Room__c = rom.get(i).Id);
			reservations.add(r);
		}

		for (Reservation__c res : reservations) {
			resController.reservation = res;
			resController.save();
		}
		for (Integer i = 0; i<rooms.size(); i++) {
			rooms.get(i).IsAvailable__c = false;

		}
		update(rooms);

		Test.startTest();

		controller.results = reservations;
		controller.beginning();

		Test.stopTest();
	}

	static testmethod void previousTest() {
		List<Reservation__c> reservations = new List<Reservation__c> ();
		Date startDate = Date.today().addDays(1);
		Date endDate = Date.today().addDays(3);
		List<Room__c> rooms = new List<Room__c> ();
		Hostel__c hostel = new Hostel__c(Name = 'Hostel', Address__c = 'Hostel', Email__c = 'hostel@gmail.com');
		insert hostel;
		List<Hostel__c> hostels = [select Id, Name from Hostel__c];
		for (Integer i = 0; i< 5; i++) {
			Room__c r = new Room__c(Name = 'Room__c ' + i, Capacity__c = i + 1, IsAvailable__c = false, Hostel__c = hostels.get(0).Id);
			rooms.add(r);
		}
		insert rooms;
		List<Room__c> rom = [select Id, Name, IsAvailable__c from Room__c];
		for (Integer i = 0; i< 4; i++) {
			Reservation__c r = new Reservation__c(Name = 'Reservation__c ' + i, Date_Occupied__c = startDate.addDays(i), Relese_Date__c = endDate.addDays(i), Room__c = rom.get(i).Id);
			reservations.add(r);
		}

		for (Reservation__c res : reservations) {
			resController.reservation = res;
			resController.save();
		}
		for (Integer i = 0; i<rooms.size(); i++) {
			rooms.get(i).IsAvailable__c = false;

		}
		update(rooms);

		Test.startTest();

		controller.results = reservations;
		controller.totalSize = reservations.size();
		controller.counter = 2;
		controller.previous();

		Test.stopTest();
	}

	static testmethod void lastTest() {
		List<Reservation__c> reservations = new List<Reservation__c> ();
		Date startDate = Date.today().addDays(1);
		Date endDate = Date.today().addDays(3);
		List<Room__c> rooms = new List<Room__c> ();
		Hostel__c hostel = new Hostel__c(Name = 'Hostel', Address__c = 'Hostel', Email__c = 'hostel@gmail.com');
		insert hostel;
		List<Hostel__c> hostels = [select Id, Name from Hostel__c];
		for (Integer i = 0; i< 5; i++) {
			Room__c r = new Room__c(Name = 'Room__c ' + i, Capacity__c = i + 1, IsAvailable__c = false, Hostel__c = hostels.get(0).Id);
			rooms.add(r);
		}
		insert rooms;
		List<Room__c> rom = [select Id, Name, IsAvailable__c from Room__c];
		for (Integer i = 0; i< 4; i++) {
			Reservation__c r = new Reservation__c(Name = 'Reservation__c ' + i, Date_Occupied__c = startDate.addDays(i), Relese_Date__c = endDate.addDays(i), Room__c = rom.get(i).Id);
			reservations.add(r);
		}

		for (Reservation__c res : reservations) {
			resController.reservation = res;
			resController.save();
		}
		for (Integer i = 0; i<rooms.size(); i++) {
			rooms.get(i).IsAvailable__c = false;

		}
		update(rooms);

		Test.startTest();

		controller.results = reservations;
		controller.totalSize = reservations.size();
		controller.last();

		Test.stopTest();
	}

	static testmethod void getDisablePreviousTest() {
	System.assertEquals(true,controller.getDisablePrevious());
	controller.counter = 2;
	System.assertEquals(false,controller.getDisablePrevious());
	}

	static testmethod void getDisableNextTest() {
	System.assertEquals(true,controller.getDisableNext());	
	controller.totalSize = 4;
	System.assertEquals(false,controller.getDisableNext());
	}
}