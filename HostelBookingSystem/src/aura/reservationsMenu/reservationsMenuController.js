({  
    dashboardClick : function(component, event) {
    	var cmpEvent = component.getEvent("menuEvent");
        console.log('cmpEvent: ' + cmpEvent);
        
        cmpEvent.setParams({"ComponentAction" : 'Dashboard' });
        
        cmpEvent.fire();
    },
    
    allReservationsClick : function(component, event) {
    	var cmpEvent = component.getEvent("menuEvent");
        console.log('cmpEvent: ' + cmpEvent);
        
        cmpEvent.setParams({"ComponentAction" : 'All_Reservations' });
        
        cmpEvent.fire();
    },
    
    newReservationClick : function(component, event) {
   		var cmpEvent = component.getEvent("menuEvent");
        console.log('cmpEvent: ' + cmpEvent);
        
        cmpEvent.setParams({"ComponentAction" : 'New_Reservation' });
        
        cmpEvent.fire();
    },
    
    homeClick : function(component, event) {
   		var cmpEvent = component.getEvent("menuEvent");
        console.log('cmpEvent: ' + cmpEvent);
        
        cmpEvent.setParams({"ComponentAction" : 'Home' });
        
        cmpEvent.fire();
    }
})