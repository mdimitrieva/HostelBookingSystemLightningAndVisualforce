({
	generateChart : function(component, event, helper) {

		 var action = component.get("c.getChartData");
        
        action.setCallback(this, function(response) {
        	var data = response.getReturnValue();  
            var dayOfWeek = [];
            var numOfResPerDay = [];
            for(var i = 0; i<data.length;i++){
                dayOfWeek.push(data[i].dayOfWeek);
                numOfResPerDay.push(data[i].numOfResPerDay);
            }
            var chartdata = {
                labels: dayOfWeek,
                datasets: [
                    {
                        label:'Line Chart showing all reservations per week. ',
                        data: numOfResPerDay,
                        borderColor:'rgba(62, 159, 222, 1)',
                        fill: false,
                        pointBackgroundColor: "#FFFFFF",
                        pointBorderWidth: 4,
                        pointHoverRadius: 5,
                        pointRadius: 3,
                        bezierCurve: true,
                        pointHitRadius: 10,
                        minimum:0 
                    }
                ]
            }
            //Get the context of the canvas element we want to select
            var ctx = component.find("lineChart").getElement();
            var lineChart = new Chart(ctx ,{
                type: 'line',
                data: chartdata,
                options: {	
                    legend: {
                        position: 'bottom',
                        padding: 10,
                    },
                    responsive: true
                }
            });
          console.log("chart data: ", chartdata);

      });
        $A.enqueueAction(action);
	}
})