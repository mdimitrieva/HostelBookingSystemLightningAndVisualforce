({
    doInit : function(component, event, helper) {
        var action = component.get("c.getRooms");
        action.setCallback(this, function(response) {
        var state = response.getState();
        if (component.isValid() && state === "SUCCESS") {
            component.set("v.rooms", response.getReturnValue());
        }
        else {
            console.log("Failed with state: " + state);
        }
    });

    // Send action off to be executed
    $A.enqueueAction(action);	 
    },
    
	createReservation : function(component, event, helper) {
		var newReservation = component.get("v.newReservation");
		helper.createReservation(component, newReservation);        
	},
    onChange : function(component, event, helper){
        var selected = component.find("resRoom").get("v.value");
        component.set("v.newReservation.Room__c", selected);
        
    },
    
    cancel : function(component, event, helper){
        component.set("v.newReservation.Name", "");
        component.set("v.newReservation.Date_Occupied__c", "");
        component.set("v.newReservation.Relese_Date__c", "");
        component.set("v.successMessage","");
        component.set("v.errorMessage","");


        component.set("v.isOpen", false);        
    }
})