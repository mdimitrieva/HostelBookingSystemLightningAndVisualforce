({
	createReservation: function(component, reservation) {
        //Save the expense and update the view
    	var action = component.get("c.save");
		
        action.setParams({
        "reservation": reservation
        });
        debugger;
        action.setCallback(this, function(response) {
            if(response.getReturnValue() !="" && response.getReturnValue() !="Reservation saved successfull!" ){
                component.set("v.errorMessage", response.getReturnValue());
            }else { 			
                component.set("v.newReservation.Name", "");
                component.set("v.newReservation.Date_Occupied__c", "");
                component.set("v.newReservation.Relese_Date__c", "");
                component.set("v.successMessage",response.getReturnValue());
                 component.set("v.isOpen", false);
            }
    	});
       	 $A.enqueueAction(action);
        
        
       
    }  

})