({
   deleteEvent : function(component, event, helper) {

    helper.deleteReservation(component, event.getParam("reservation"));
	},
        
	searchKeyChange: function(component, event) {
        var searchKey = event.getParam("searchKey");
        var action = component.get("c.findByName");
        action.setParams({
          "searchKey": searchKey
        });
        action.setCallback(this, function(a) {
            component.set("v.reservations", a.getReturnValue());
        });
        $A.enqueueAction(action);
	},  
    
    sortName: function(component, event, helper) {
      helper.sortBy(component, "Name");
    },
    sortOccupiedDate: function(component, event, helper) {
      helper.sortBy(component, "Date_Occupied__c");
    },
    sortReleseDate: function(component, event, helper) {
     helper.sortBy(component, "Relese_Date__c");
    },
    sortDuration: function(component, event, helper) {
    helper.sortBy(component, "Duration__c"); 
    },
    
     doInit: function(component, event, helper) {
      
      var page = component.get("v.page") || 1;

      var recordToDisply = component.find("recordSize").get("v.value");

      helper.getReservations(component, page, recordToDisply);
 
   },
 
   previousPage: function(component, event, helper) {

      var page = component.get("v.page") || 1;

      var direction = event.getSource().get("v.label");

      var recordToDisply = component.find("recordSize").get("v.value");

      page = direction === "Previous Page" ? (page - 1) : (page + 1);
       
      helper.getReservations(component, page, recordToDisply);
 
   },
 
   nextPage: function(component, event, helper) {

      var page = component.get("v.page") || 1;

      var direction = event.getSource().get("v.label");

      var recordToDisply = component.find("recordSize").get("v.value");

      page = direction === "Previous Page" ? (page - 1) : (page + 1);
       
      helper.getReservations(component, page, recordToDisply); 
   },
 
   onSelectChange: function(component, event, helper) {

      var page = 1
      var recordToDisply = component.find("recordSize").get("v.value");
	  
      helper.getReservations(component, page, recordToDisply);
   }
   
})