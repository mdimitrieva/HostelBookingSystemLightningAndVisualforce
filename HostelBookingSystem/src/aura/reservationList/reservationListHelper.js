({
	 sortHelper: function(component) {
      var currentDir = component.get("v.arrowDirection");
 
      if (currentDir == 'arrowdown') {
         component.set("v.arrowDirection", 'arrowup');
         component.set("v.isAsc", true);
      } else {
         component.set("v.arrowDirection", 'arrowdown');
         component.set("v.isAsc", false);
      }

   },
    
    deleteReservation : function(component, reservation, callback) {

    var action = component.get("c.remove");
    action.setParams({
        "reservation": reservation
    });
    action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
            // Remove only the deleted expense from view
            var reservations = component.get("v.reservations");
            var items = [];
            for (var i = 0; i < reservations.length; i++) {
                if(reservations[i]!==reservation) {
                    items.push(reservations[i]);  
                }
            }
            component.set("v.reservations", items);
           
        }
    });
    $A.enqueueAction(action);
         var page = component.get("v.page") || 1; 
      var recordToDisply = component.find("recordSize").get("v.value");

      page  === "Previous Page" ? (page - 1) : (page + 1);

        this.getReservations(component, page, recordToDisply);
	},
    
    getReservations: function(component, page, recordToDisply,orderBy,ascOrDesc) {

      var action = component.get("c.pagingResults");
		
      action.setParams({
         "pageNumber": page,
         "recordToDisply": recordToDisply,
         "sortBy": orderBy,
         "isAsc": ascOrDesc
      });

      action.setCallback(this, function(a) {

         var result = a.getReturnValue();
          
         component.set("v.reservations", result.reservations);
         component.set("v.page", result.page);
         component.set("v.total", result.total);
         component.set("v.pages", Math.ceil(result.total / recordToDisply));
 
      });

      $A.enqueueAction(action);
   },
    sortBy: function(component, field) {
        this.sortHelper(component);
        var sortAsc = component.get("v.sortAsc"),
            sortField = component.get("v.sortField"),
            records = component.get("v.reservations");
        sortAsc = field == sortField? !sortAsc: true;
        records.sort(function(a,b){
            var t1 = a[field] == b[field],
                t2 = a[field] > b[field];
            return t1? 0: (sortAsc?-1:1)*(t2?-1:1);
        });
        component.set("v.sortAsc", sortAsc);
        component.set("v.sortField", field);
        component.set("v.reservations", records);
    }
})