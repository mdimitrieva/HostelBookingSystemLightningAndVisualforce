        function getParamValuesByName (querystring) {
          var qstring = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
          for (var i = 0; i < qstring.length; i++) {
            var urlparam = qstring[i].split('=');
            if (urlparam[0] == querystring) {
               return urlparam[1];
            }
          }
        }

        var id = getParamValuesByName('id');
        if(id!=null){
        console.log('id is not null');
            displayAddEditSection();
        }

        function displayAddEditSection() {
            var x= document.getElementById('addAndEditSection').style;
            var y = document.getElementById('allReservation').style;
            var z = document.getElementById('dashboard').style;
                if(x.display == 'none'){
                    x.display = "block";
                    y.display = "none";
                    z.display = "none";
                }
        }

        function displayAllSection() {

            var x= document.getElementById('addAndEditSection').style;
            var y = document.getElementById('allReservation').style;
            var z = document.getElementById('dashboard').style;
                if(y.display == 'none'){
                    y.display = "block";
                    x.display = "none";
                    z.display = "none";
                }

        }

         function displayDashboardSection() {
             var x= document.getElementById('addAndEditSection').style;
             var y = document.getElementById('allReservation').style;
             var z = document.getElementById('dashboard').style;
                if(z.display == 'none'){
                    z.display = "block";
                    x.display = "none";
                    y.display = "none";
                }
        }
